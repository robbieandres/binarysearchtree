//Written by Robbie Andres
//CS1120

#include "int-bst.h"

//bool = Boolean so returns true or false
// struct int_bst_nodei
bool int_bst_insert(int_bst_node_t **t_p, int n) { //* creates pointer *t_p  ** = handle when you want to change point
  //where you add value & create children 
  if(*t_p == NULL) {
    (*t_p) = (int_bst_node_t *) malloc(sizeof (int_bst_node_t)); //creates new node
    (**t_p).data = n;  //data = center node
    (**t_p).left = NULL; //create a left child
    (**t_p).right = NULL; //create a right child
    return true; //yes you can insert value
  } else if((**t_p).data > n) {   //if n is < root
      return int_bst_insert(&((**t_p).left), n);//& references the left node, making it our root
  } else if((**t_p).data < n) { //if n is greater than pointer *t_p 
      return int_bst_insert(&((**t_p).right), n);//makes right node our root
  } else {
      return false; //value equal to root or non int return false
  }
}

bool int_bst_find(int_bst_node_t *t, int n) {
  if((*t).data == n) {  //pointer *t -> data is equal to value looking for
    return true;  //value is present
  } else if((*t).data > n) { //if value looking for is less than root value
    if((*t).left == NULL) {  //if left node is empty
      return false;  //return false
    } else {  
      return int_bst_find(((*t).left), n); //make left node root
    }
  } else if((*t).data < n) {
    if((*t).right == NULL) {  //if right node is empty
      return false;  //return false
    } else {  
      return int_bst_find(((*t).right), n); //make left node root
    } 
  } else {
    return false;
    printf("wrong input");
  }
}

void int_bst_remove(int_bst_node_t ** t_p, int n) {
 /* if(t_p == NULL); {
    return;
  } else if((**t_p).data > n) {
    int_bst_remove((**t_p).right, n);
    return;
  } else if((**t_p).data < n) {
    int_bst_remove((**t_p).left, n);
    return;
  } else {
    if(t_p == n) {
      fprintf(0,"%d \n", t_p -> data);
      return;
    }
  return;    
  }  */
}

void int_bst_remove_all(int_bst_node_t **t_p) {
  if(t_p == NULL) {  //if root value = NULL
    return ;
  } else {
    t_p = NULL ;  //make value data = NULL
    int_bst_remove_all(&((**t_p).left)); //sends function to delete values on left
    int_bst_remove_all(&((**t_p).right));//makes values on right = NULL
    return;
  }
  //&(*t_p) follow pointer/ give address of pointer  **t_p pointer to pointer
}

void int_bst_print_elements(int_bst_node_t * t, FILE * f, char * fmt) {
  if(t==NULL) return;  
  int_bst_print_elements((*t).left, f, fmt);
  fprintf(f,fmt,t -> data); 
  int_bst_print_elements((*t).right, f, fmt);
  return;
}

void help_print_tree(int_bst_node_t * t, FILE * f, int j) {
  for(int i = 0; i<j; i++) {
    fprintf(f,"\t");
  } 
  if(t != NULL) {
    fprintf(f,"%d \n", t-> data);
    j++;
    help_print_tree((*t).left, f,j);
    help_print_tree((*t).right, f,j);
    j--;
  } else fprintf(f, ". \n");

}

void int_bst_print_as_tree(int_bst_node_t * t, FILE * f) {
  if(t!=NULL) {
    help_print_tree(t, f,0 );
    return;
  }
}

